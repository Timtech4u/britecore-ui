import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        endpoint: "https://tim-bcore.herokuapp.com/risks/",
        responsesEndpoint: "https://tim-bcore.herokuapp.com/risks/responses/",
        formSchema: {
            groups: [{
                fields: [],
                legend: ''
            }]
        },
        forms: [],
        models: {},
    },
    mutations: {
        updateModel(state, newModel) {
            state.models = newModel
        },
        updateFormSchema(state, newSchema) {
            state.formSchema.groups[0].fields = newSchema.fields
            state.formSchema.groups[0].legend = newSchema.insurerName
        },
        updateForms(state, payload) {
            payload.forEach((element, index) => {
                state.forms[index] = {}
                state.forms[index].name = element.name
                state.forms[index].id = element.id
                state.forms[index].formSchema = { groups: [{ fields: element.fields, legend: element.name }] }
                state.forms[index].responses = element.responses.map(row => {
                    let rowObj = {}
                    for (let key in row.response) {
                        rowObj[key.split(' ').join('')] = row.response[key].toString()
                    }
                    return rowObj
                })
                state.forms[index].columns = element.fields.map(field => ({ label: field.name, field: field.model.split(' ').join(''), type: field.type }))
            })
        }
    },
    getters: {
        formSchema: state => state.formSchema,
        endpoint: state => state.endpoint,
        responsesEndpoint: state => state.responsesEndpoint,
        models: state => state.models,
        forms: state => state.forms
    }
})