# BriteCore Frontend Repository for Risk API


## Detailed Documentation [here](https://gitlab.com/Timtech4u/britecore)


## Frontend Deployed [here](http://tim-britecore.s3-website-us-east-1.amazonaws.com)



## Project setup:
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```
